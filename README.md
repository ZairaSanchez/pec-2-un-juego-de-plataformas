# PEC 2 - Un juego de plataformas

## FUNCIONAMIENTO

El juego empieza con la pantalla inicial (Pantalla Principal) donde aparecen dos botones:

- "Nueva partida", que, si lo pulsamos, comenzaremos una partida, llevándonos a la pantalla del juego propiamente dicho (Nivel 1-1).
- "Salir", que cierra la aplicación.

En la segunda pantalla (Nivel 1-1), manejamos a Mario, que puede:

- Correr y saltar (con varias intensidades).
- Golpear bloques de interrogación y obtener de ellos monedas o champiñones. El champiñón rojo le hace crecer y el verde le da una vida.
- Golpear goombas por encima para matarlos.
- Morir si toca a un goomba por los lados o si cae por un abismo.
- Tocar la bandera o el asta y completar el nivel.

Cuando Mario se queda sin vidas o completa el nivel, se vuelve a la Pantalla principal.


## ESTRUCTURA

1. Creación de pantalla inicial (Pantalla Principal):
- Adición de fondo.
- Adición de música ("Tema principal").
- Creación del botón Nueva partida. 
	i. Implementación de funcionalidad (abrir pantalla Nivel 1-1 – script NuevaPartida).
- Creación del botón Salir.
	i. Implementación de funcionalidad (salir de la aplicación – script SalirJuego). 

2. Creación de pantalla de juego (Nivel 1-1).
- Adición de fondo.
- Adición de música.
	i.  Al iniciar la pantalla suena "Tema principal".
	ii. Si el jugador ha completado el nivel, suena "super_mario_bros_music_level_complete".
	iii. Si el jugador ha muerto, suena "music_new_super_mario_bros_dead".
- Creación de tilemap.
- Creación de Mario pequeño, grande, muerto y de las animaciones que conlleva correr, saltar, crecer, decrecer y morir (scripts TocaSuelo, Movimiento, SaltoMejorado, Muerte).
- Creación de goombas y de las animaciones que conlleva su movimiento y su muerte (script GolpeGoomba).
- Seguimiento de Mario por parte de la cámara (script MovimientoCamara).
- Creación de los bloques de ladrillo y de interrogación, y la funcionalidad de cuando son golpeados (script GolpeBloque).
- Creación de las monedas y de los champiñones, y de la funcionalidad de cuando son cogidos (script CogeObjeto).
- Creación de los muros invisibles que impiden salirse del juego (MuroInicial y MuroFinal), y los que permiten morir (MuroInferior) o completar el nivel (Meta) (scripts GolpeMuro, MuerteCaida y TocaBandera).
- Creación de los objetos que muestran las monedas obtenidas y las vidas.
- Creación de la lógica del juego (script ControladorJuego).
