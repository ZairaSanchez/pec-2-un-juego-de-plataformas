﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaltoMejorado : MonoBehaviour
{
    Rigidbody2D rigidBody2D;
    public float fallMultiplier;
    public float lowJumpMultiplier;

    void Awake()
    {
        rigidBody2D = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        //Salto mejorado
        if (rigidBody2D.velocity.y < 0)
            rigidBody2D.velocity += Vector2.up * Physics2D.gravity.y * (fallMultiplier) * Time.deltaTime;
        else if (rigidBody2D.velocity.y > 0 && !Input.GetKey(KeyCode.Space))
            rigidBody2D.velocity += Vector2.up * Physics2D.gravity.y * (lowJumpMultiplier) * Time.deltaTime;
    }
}
