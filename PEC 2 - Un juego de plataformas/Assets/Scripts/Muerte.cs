﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Muerte : MonoBehaviour
{
    private ControladorJuego controlador;

    void Start()
    {
        controlador = GameObject.Find("Controlador").GetComponent<ControladorJuego>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            //Si tocamos un goomba por los lados, morimos
            controlador.Morir();
        }
    }
}
