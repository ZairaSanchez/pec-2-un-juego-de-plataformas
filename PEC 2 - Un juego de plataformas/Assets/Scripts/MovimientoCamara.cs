﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Security.Cryptography;
using UnityEngine;

public class MovimientoCamara : MonoBehaviour
{
    public Transform jugador;

    private void Update()
    {
        //Seguimos horizontalmente al jugador
        transform.position = new Vector3(jugador.position.x, transform.position.y, transform.position.z);
    }
}
