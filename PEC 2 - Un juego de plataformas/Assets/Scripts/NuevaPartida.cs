﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NuevaPartida : MonoBehaviour
{
    //Empezamos una nueva partida
    public void PartidaNueva()
    {
        //Empezamos con 3 vidas
        PlayerPrefs.SetInt("Vidas", 3);
        SceneManager.LoadScene("Nivel 1-1");
    }
}
