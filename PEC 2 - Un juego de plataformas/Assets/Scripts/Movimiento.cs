﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class Movimiento : MonoBehaviour
{
	public float velocidadCorrer;
	public float velocidadSalto;
	public Animator animador;
	public Transform jugador;

	private Rigidbody2D rigidBody2D;
	private SpriteRenderer spriteRenderer;
	private Vector3 distancia;

	// Start is called before the first frame update
	void Start()
	{
		//Inicialización de variables
		rigidBody2D = GetComponent<Rigidbody2D>();
		spriteRenderer = GetComponent<SpriteRenderer>();
		distancia = jugador.transform.position - transform.position;
	}

    void FixedUpdate()
	{
		//Movimiento del personaje
		transform.position = jugador.position - distancia;

        //Izquierda
        if (Input.GetKey(KeyCode.LeftArrow))
			MueveIzquierda();
		//Derecha
		else if (Input.GetKey(KeyCode.RightArrow))
			MueveDerecha();
		else
		{
			//Mario está quieto
            rigidBody2D.velocity = new Vector2(0, rigidBody2D.velocity.y);
			animador.SetBool("Corre", false);
        }

		//Salto del personaje
		if (Input.GetKey(KeyCode.Space) && TocaSuelo.tocaSuelo)
			Salta();

		//Animación salto
		if (!TocaSuelo.tocaSuelo)
		{
			animador.SetBool("Salta", true);
            animador.SetBool("Corre", false);
        }
		if (TocaSuelo.tocaSuelo)
            animador.SetBool("Salta", false);
	}

	//El personaje se mueve hacia la izquierda
	public void MueveIzquierda()
	{
		rigidBody2D.velocity = new Vector2(-velocidadCorrer, rigidBody2D.velocity.y);
		spriteRenderer.flipX = true;
		animador.SetBool("Corre", true);
	}

	//El personaje se mueve hacia la derecha
	public void MueveDerecha()
	{
		rigidBody2D.velocity = new Vector2(velocidadCorrer, rigidBody2D.velocity.y);
		spriteRenderer.flipX = false;
		animador.SetBool("Corre", true);
	}

	//El personaje salta
	public void Salta()
	{
        rigidBody2D.velocity = new Vector2(rigidBody2D.velocity.x, velocidadSalto);
	}
}
