﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CogeObjeto : MonoBehaviour
{
	public ControladorJuego controlador;

	private void OnTriggerEnter2D(Collider2D collision)
	{
		//Mario coge un objeto
		if (collision.CompareTag("Player"))
		{
			//Champiñón rojo
			if (gameObject.transform.parent.name == "BloqueChampinyonRojo")
				controlador.cogerSuperChampinyon();
			//Champiñón verde
			if (gameObject.transform.parent.name == "BloqueChampinyonVerde")
				controlador.cogerChampinyonVerde();

			//Destruimos los objetos
			StartCoroutine("DestruyeChampinyon");
		}
	}

	IEnumerator DestruyeChampinyon()
	{
		//Esperamos
		yield return new WaitForSeconds(0.5f);

		//Ocultamos el champiñón
		gameObject.SetActive(false);
	}
}
