﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GolpeGoomba : MonoBehaviour
{
	public Animator animadorGoomba;
	private ControladorJuego controlador;

	void Start()
    {
		controlador = GameObject.Find("Controlador").GetComponent<ControladorJuego>();
	}

    private void OnTriggerEnter2D(Collider2D collision)
	{
		//Mario golpea a un Goomba
		if (collision.CompareTag("Player"))
			StartCoroutine("DestruyeGoomba");
	}

	//Destruimos el Goomba
	IEnumerator DestruyeGoomba()
    {
		controlador.golpeGoomba();

		//Animación de muerte del Goomba
		animadorGoomba.SetBool("Muerto", true);
		yield return new WaitForSeconds(0.1f);
		animadorGoomba.SetBool("Muerto", false);
		yield return new WaitForSeconds(0.1f);

		//Destruimos el objeto del Goomba
		Destroy(animadorGoomba.gameObject);
	}
}
