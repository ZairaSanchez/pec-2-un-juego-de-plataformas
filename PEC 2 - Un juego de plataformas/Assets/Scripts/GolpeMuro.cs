﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GolpeMuro : MonoBehaviour
{
	private ControladorJuego controlador;

	void Start()
	{
		controlador = GameObject.Find("Controlador").GetComponent<ControladorJuego>();
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.CompareTag("Player"))
		{
			//Mario golpea un muro invisible
			if (gameObject.name == "MuroInicial")
				controlador.golpeMuroInicial();
			else if (gameObject.name == "MuroFinal")
				controlador.golpeMuroFinal();
		}
	}
}
