﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Runtime.Versioning;

public class ControladorJuego : MonoBehaviour
{
    public Animator animadorMario;
    public AudioSource fuenteAudio;
    public AudioClip nivelCompleto;
    public AudioClip muerte;

    private Rigidbody2D rigidBodyMario;
    private BoxCollider2D boxColliderMario;
    private Text mensaje;
    private Text numeroVidas;
    private int numVidas;
    private GameObject menu;
    private GameObject tocaSuelo;

    // Start is called before the first frame update
    void Start()
    {
        //Inicialización de variables
        rigidBodyMario = animadorMario.gameObject.GetComponent<Rigidbody2D>();
        boxColliderMario = animadorMario.gameObject.GetComponent<BoxCollider2D>();
        mensaje = GameObject.Find("Mensaje").GetComponent<Text>();
        numeroVidas = GameObject.Find("NumeroVidas").GetComponent<Text>();
        numVidas = 3;
        menu = GameObject.Find("Menu");
        tocaSuelo = GameObject.Find("TocaSuelo");

        //Si tenemos vidas negativas, las inicializamos a 3
        if (PlayerPrefs.GetInt("Vidas") <= 0)
            PlayerPrefs.SetInt("Vidas", numVidas);
        //Si hemos muerto, disminuye nuestro número de vidas
        else numVidas = PlayerPrefs.GetInt("Vidas");
        numeroVidas.text = numVidas.ToString();
    }

    //Mario golpea un goomba
    public void golpeGoomba()
    {
        rigidBodyMario.velocity = new Vector2(rigidBodyMario.velocity.x, 8f);
    }

    //Mario golpea el muro inicial
    public void golpeMuroInicial()
    {
        rigidBodyMario.MovePosition(rigidBodyMario.position + Vector2.right * 5f * Time.deltaTime);
    }

    //Mario golpea el muro final
    public void golpeMuroFinal()
    {
        rigidBodyMario.MovePosition(rigidBodyMario.position - Vector2.right * 5f * Time.deltaTime);
    }

    //Mario coge un champiñón rojo, que le hace crecer
    public void cogerSuperChampinyon()
    {
        //Hacemos la animación de crecer
        StartCoroutine("Crecer");
    }

    //Mario coge un champiñón verde, ganando una vida
    public void cogerChampinyonVerde()
    {
        //Aumentamos en uno el número de vidas
        numVidas++;
        numeroVidas.text = numVidas.ToString();
    }

    //Mario toca la bandera
    public void tocaBandera()
    {
        //Mario ha completado el nivel
        mensaje.text = "Has ganado";
        StartCoroutine("FinalGanador");

        //Ejecutamos el clip
        fuenteAudio.clip = nivelCompleto;
        fuenteAudio.Play();
    }

    //Mario muere o decrece
    public void Morir()
    {
        //Si Mario es grande, decrece
        if (animadorMario.GetBool("Crece"))
            StartCoroutine("Decrecer");
        //Si Mario es pequeño, muere
        else
        {
            //Disminuimos en uno el número de vidas
            numVidas--;

            //Se notifica que Mario ha muerto y se actualiza el número de vidas
            mensaje.text = "Has muerto";
            numeroVidas.text = numVidas.ToString();

            //Animación de muerte
            animadorMario.SetTrigger("Muerto");

            //Saltamos un poco
            rigidBodyMario.velocity = new Vector2(rigidBodyMario.velocity.x, 8f);
            boxColliderMario.enabled = false;

            //Ejecutamos el clip
            fuenteAudio.clip = muerte;
            fuenteAudio.Play();

            //Se va a la pantalla principal o se reinicia el nivel
            StartCoroutine("FinalMuerte");
        }
    }

    //Mario muere por caerse
    public void MorirCaida()
    {
        //Disminuimos en uno el número de vidas
        numVidas--;

        //Se notifica que Mario ha muerto y se actualiza el número de vidas
        mensaje.text = "Has muerto";
        numeroVidas.text = numVidas.ToString();

        //Animación de muerte
        animadorMario.SetTrigger("Muerto");

        //Modificamos el scale del transform para adecuarlo a la nueva imagen de Mario pequeño
        rigidBodyMario.transform.localScale = new Vector2(rigidBodyMario.transform.localScale.x, 4.5f);

        //Saltamos un poco
        rigidBodyMario.velocity = new Vector2(rigidBodyMario.velocity.x, 8f);
        boxColliderMario.enabled = false;

        //Ejecutamos el clip
        fuenteAudio.clip = muerte;
        fuenteAudio.Play();

        //Se va a la pantalla principal o se reinicia el nivel
        StartCoroutine("FinalMuerte");
    }

    //Animación de crecer
    IEnumerator Crecer()
    {
        //Modificamos el scale del transform para adecuarlo a la nueva imagen de Mario grande
        rigidBodyMario.transform.localScale = new Vector2(rigidBodyMario.transform.localScale.x,  8.5f);
        //Esperamos
        yield return new WaitForSeconds(0.1f);
        //Mario crece
        animadorMario.SetBool("Crece", true);
        //Esperamos
        yield return new WaitForSeconds(0.1f);
        //Saltamos un poco
        rigidBodyMario.velocity = new Vector2(rigidBodyMario.velocity.x, 6f);
    }

    //Animación de decrecer
    IEnumerator Decrecer()
    {
        //Modificamos el scale del transform para adecuarlo a la nueva imagen de Mario pequeño
        rigidBodyMario.transform.localScale = new Vector2(rigidBodyMario.transform.localScale.x, 4.5f);
        //Esperamos
        yield return new WaitForSeconds(0.1f);
        //Mario decrece
        animadorMario.SetBool("Crece", false);
        //Esperamos
        yield return new WaitForSeconds(0.1f);
        //Saltamos un poco
        rigidBodyMario.velocity = new Vector2(rigidBodyMario.velocity.x, 6f);
    }

    //Se vuelve a la pantalla principal o se reinicia el nivel
    IEnumerator FinalMuerte()
    {
        //Esperamos
        yield return new WaitForSeconds(2.5f);

        //Si no nos quedan vidas, volvemos a la pantalla principal
        if (numVidas == 0)
        {
            VidasACero();
            yield return new WaitForSeconds(1f);

            //Volvemos a la pantalla principal
            SceneManager.LoadScene("Pantalla principal");
        }
        //Si aún nos quedan vidas, las apuntamos y reiniciamos el nivel
        else
        {
            PlayerPrefs.SetInt("Vidas", numVidas);
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }

    //Se vuelve a la pantalla principal
    IEnumerator FinalGanador()
    {
        rigidBodyMario.MovePosition(rigidBodyMario.position - Vector2.down * 2f * Time.deltaTime);

        //Esperamos
        yield return new WaitForSeconds(7f);
        
        //Volvemos a la pantalla principal
        SceneManager.LoadScene("Pantalla principal");
    }

    //Comprueba si nos hemos quedado sin vidas
    private void VidasACero()
    {
        //Si no nos quedan vidas, se muestra el mensaje de que se ha perdido la partida
        if (numVidas == 0)
            mensaje.text = "Has perdido";
    }
}
