﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SalirJuego : MonoBehaviour
{
    //Cerramos la aplicación
    public void Salir()
    {
        Application.Quit();
    }
}
