﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TocaSuelo : MonoBehaviour
{
	public static bool tocaSuelo;

	//Entra en colisión
	private void OnTriggerEnter2D(Collider2D collision)
	{
		tocaSuelo = true;
	}

	//Sale de colisión
	private void OnTriggerExit2D(Collider2D collision)
	{
		tocaSuelo = false;
	}
}
