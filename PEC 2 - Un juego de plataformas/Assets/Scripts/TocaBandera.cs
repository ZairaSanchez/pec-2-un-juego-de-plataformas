﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TocaBandera : MonoBehaviour
{
	private ControladorJuego controlador;

	void Start()
	{
		controlador = GameObject.Find("Controlador").GetComponent<ControladorJuego>();
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.CompareTag("Player"))
		{
			//Mario toca la bandera y completa el nivel
			controlador.tocaBandera();
		}
	}
}
