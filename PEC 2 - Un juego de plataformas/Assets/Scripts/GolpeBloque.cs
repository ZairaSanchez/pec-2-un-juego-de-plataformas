﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GolpeBloque : MonoBehaviour
{
	public GameObject moneda;
	public GameObject premio;
	private Text numeroMonedas;
	private int numMonedas;

	// Start is called before the first frame update
	void Start()
	{
		numeroMonedas = GameObject.Find("NumeroMonedas").GetComponent<Text>();
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		//Mario golpea un bloque
		if (collision.CompareTag("Player"))
		{
			//Champiñón
			if (premio != null) premio.SetActive(true);
			//Moneda
			else moneda.SetActive(true);

			//Destruimos las monedas
			StartCoroutine("DestruirMonedas");			
		}
	}

	//Destruye las monedas y cambia el bloque
	IEnumerator DestruirMonedas()
	{
		//Ocultamos la imagen de la interrogación para mostrar el bloque usado
		gameObject.GetComponent<SpriteRenderer>().enabled = false;
		gameObject.GetComponent<Collider2D>().enabled = false;

		//Esperamos
		yield return new WaitForSeconds(0.5f);

		if (premio == null)
		{
			//Aumentamos en uno el número de monedas
			numMonedas = 0;
			int.TryParse(numeroMonedas.text, out numMonedas);
			numMonedas++;
			numeroMonedas.text = numMonedas.ToString();

			//Ocultamos la moneda
			moneda.SetActive(false);
		}
	}
}
